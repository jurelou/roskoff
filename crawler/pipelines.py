from core.models import Website
from core.models import Link
from core.database.services import WebsiteService, LinkService

from scrapy.exceptions import DropItem


class LinksPipeline:
    def process_item(self, item, spider):
        if not isinstance(item, Link):
            return item
        LinkService.index(body=item)
        raise DropItem(f"Done with link {item}")


class WebsitePipeline:
    def process_item(self, item, spider):
        if not isinstance(item, Website):
            return item
        # print(f"found website: {item.url}")
        WebsiteService.index(body=item)
        raise DropItem(f"Done with website {item}")
