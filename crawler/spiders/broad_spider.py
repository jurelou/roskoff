import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from bs4 import BeautifulSoup
import re

from core.models import Link
from core.models import Feed
from core.models import Relationship
from core.models import Website
from core.helpers.tor import find_onions, is_onion


class BroadSpider(scrapy.Spider):
    name = "broad"

    custom_settings = {"DEPTH_LIMIT": -1}  # Only first page

    _extractor = LinkExtractor(unique=True)
    _feed_name = Feed.BROAD_CRAWLING
    _follow_onions = False

    def _link_ext(self, response):
        return [l.url for l in self._extractor.extract_links(response)]

    def yield_data(self, source_url, target_url, relationship, follow=True):
        # 542 chars in a link ????
        if len(target_url) > 542:
            return
        yield Link(source=source_url, target=target_url, relationship=relationship)
        yield Website(url=target_url, source=self._feed_name)
        onion = is_onion(target_url)

        if follow:
            if onion and self._follow_onions:
                yield scrapy.Request(
                    url=target_url, callback=self.parse, errback=self.errback
                )
            elif not onion:
                yield scrapy.Request(
                    url=target_url, callback=self.parse, errback=self.errback
                )

    def parse(self, response):
        print(f"-> {response.url}")
        yield Website(url=response.url, source=self._feed_name)

        if not hasattr(response, "text"):
            return

        extracted_links = self._link_ext(response)
        print(f"Found {len(extracted_links)} links from {response.url}")
        for L in extracted_links:
            # follow = False if self.custom_settings["DEPTH_LIMIT"] == -1 else True
            yield from self.yield_data(
                source_url=response.url,
                target_url=L,
                relationship=Relationship.STRAIGHT,
                follow=True,
            )

        _onions = find_onions(response.text)
        print(f"Found {len(_onions)} onions from {response.url}")
        for onion_link in _onions:
            if onion_link in extracted_links:
                continue
            yield from self.yield_data(
                source_url=response.url,
                target_url=onion_link,
                relationship=Relationship.DERIVED,
                follow=False,
            )

    def errback(self, error):
        print("Error", error)
