import scrapy
from crawler.spiders import BroadSpider
from core.models import Feed


class TestingSpider(BroadSpider):
    name = "testingSpider"
    custom_settings = {"DEPTH_LIMIT": -1}

    def __init__(self, url, **kwargs):
        self.start_urls = [url]
        super().__init__(**kwargs)
