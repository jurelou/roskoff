import scrapy
from scrapy import Selector
from scrapy.linkextractors import LinkExtractor
import re

from core.models import Link
from core.models import Feed
from core.models import Relationship
from core.models import Website

from core.helpers.tor import find_onions


class HackerNewsSpider(scrapy.Spider):
    name = "hackerNews"
    custom_settings = {"DEPTH_LIMIT": 10}

    _base_url = "https://news.ycombinator.com/"
    start_urls = [
        "https://news.ycombinator.com/news",
        "https://news.ycombinator.com/newest",
        "https://news.ycombinator.com/front",
        "https://news.ycombinator.com/newcomments",
        "https://news.ycombinator.com/ask",
    ]

    _extractor_more = LinkExtractor(restrict_xpaths=('//a[text()="More"]',))

    _extractor = LinkExtractor(restrict_css=("[class=storylink]"))

    def parse(self, response, follow=True):
        sel = Selector(text=response.body, type="html")
        comments = sel.xpath("//td[contains(@class, 'subtext')]/a[3]/@href").extract()
        for comment in comments:
            yield scrapy.Request(
                url=self._base_url + comment, callback=self.parse, errback=self.errback
            )

        if follow:
            for L in self._extractor_more.extract_links(response):
                yield scrapy.Request(
                    url=L.url, callback=self.parse, errback=self.errback
                )
            for L in self._extractor.extract_links(response):
                yield scrapy.Request(
                    url=L.url,
                    callback=self.parse,
                    errback=self.errback,
                    cb_kwargs={"follow": False},
                )

        for onion_link in find_onions(response.text):
            yield Website(url=response.url, source=Feed.HACKER_NEWS)
            yield Link(
                source=response.url,
                target=onion_link,
                relationship=Relationship.DERIVED,
            )

    def errback(self, error):
        pass
