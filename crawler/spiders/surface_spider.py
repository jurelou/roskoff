import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from crawler.spiders import BroadSpider
from core.models import Feed

SPIDERS_DATA = {}
SPIDERS_DEPTHS = []


class SurfaceSpider(BroadSpider):
    name = "surface"

    def start_requests(self):
        for url in SPIDERS_DATA[self.settings["DEPTH_LIMIT"]]:
            yield scrapy.Request(url=url, callback=self.parse, errback=self.errback)

    @classmethod
    def update_settings(cls, settings):
        todo = SPIDERS_DEPTHS.pop(0)
        print(settings.__dict__)
        settings.setdict({"DEPTH_LIMIT": todo}, priority="spider")

    @classmethod
    def get_crawler_process(cls):
        with open("./misc/surface_seeds.txt", "r") as f:
            for raw_line in f.readlines():
                line = raw_line.rstrip().split()
                if len(line) != 2 or raw_line.startswith("#"):
                    continue

                depth = int(line[0].split("depth=")[1])
                if depth == -1:
                    depth = 0
                elif depth == 0:
                    depth = -1
                url = line[1].split("url=")[1]

                SPIDERS_DEPTHS.append(depth)
                if depth in SPIDERS_DATA:
                    SPIDERS_DATA[depth].append(url)
                else:
                    SPIDERS_DATA[depth] = [url]

        process = CrawlerProcess(get_project_settings())
        for _ in range(len(SPIDERS_DATA)):
            # print("ONE", depth, urls)
            # SurfaceSpider.custom_settings = {"DEPTH_LIMIT": depth}
            # SurfaceSpider.start_urls = urls
            process.crawl(cls.name)
        return process
