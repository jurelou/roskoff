import asyncio
import os
import aiohttp
from aiohttp.web import HTTPException
from aiohttp.client_exceptions import ClientError
from dynaconf import settings
from core.database.services import WebsiteService, OnionWebsiteService
from core.models import OnionWebsite


async def output_consumer(main_queue, dlq):
    while True:
        try:
            response = await main_queue.get()
            onion = OnionWebsite(**response)
            OnionWebsiteService.index(onion)
            OnionWebsiteService.refresh()
            print(f"-----> {response['http_version']} {response['url']}")
            main_queue.task_done()
        except Exception as err:
            print("!!!", err)

async def dlq_consumer(main_queue, dlq, session, out_queue):
    while True:
        url, err = await main_queue.get()
        print(f"ERROR {url} ->({type(err).__name__}) {err}")
        main_queue.task_done()


async def extract_response(response):
    return {
        "http_version": ".".join([str(response.version.major), str(response.version.minor)]),
        "status": response.status,
        "html": await response.text(),
        "url": str(response.url),
        "real_url": str(response.real_url),
        # "headers": response.raw_headers,
        "content_type": response.content_type,
        "charset": response.charset,
    }


async def retry_consumer(main_queue, dlq, session, out_queue):
    while True:
        url = await main_queue.get()
        try:
            async with session.get(
                url, timeout=settings.PROBE.REQUEST_TIMEOUT, raise_for_status=True
            ) as response:
                response_data = await extract_response(response)
                await out_queue.put(response_data)
        except Exception as err:
            await dlq.put((url, err))
        main_queue.task_done()


async def consumer(main_queue, retry_queue, dlq, session, out_queue):
    while True:
        url = await main_queue.get()
        try:
            async with session.get(
                url, timeout=settings.PROBE.REQUEST_TIMEOUT, raise_for_status=True
            ) as response:
                response_data = await extract_response(response)
                await out_queue.put(response_data)
        # except asyncio.TimeoutError as err:
        #     await dlq.put((url, err))
        except ClientError as err:
            retry_url = (
                f"https://{url[7:]}"
                if url.startswith("http://")
                else f"http://{url[8:]}"
            )
            print(f"RETRY {url} -> {retry_url}")
            await retry_queue.put(retry_url)
        except Exception as err:
            await dlq.put((url, err))

        main_queue.task_done()


async def produce(queue):
    WebsiteService.refresh()
    onions = WebsiteService.scroll_onions()
    for onion in onions:
        url = onion["_source"]["url"]
        if not url.startswith("http"):
            url = f"http://{url}"
        print("Push", url)
        await queue.put(url)

    # await queue.put(f"http://monopolyberbucxuqqqq.onion")

    # await queue.put(f"http://monopolyberbucxu.onion")
    # await queue.put(f"https://monopolyberbucxu.onion")

    # await queue.put(f"https://www.facebookcorewwwi.onion")
    # await queue.put(f"http://www.facebookcorewwwi.onion")

    # await queue.put(f"https://facebookcorewwwi.onion")
    # await queue.put(f"http://facebookcorewwwi.onion")

    # await queue.put(f"https://google.fr")
    # await queue.put(f"http://google.fr")


async def launch(session):
    main_queue = asyncio.Queue(maxsize=settings.PROBE.QUEUE_MAX_SIZE)
    retry_queue = asyncio.Queue(maxsize=settings.PROBE.QUEUE_MAX_SIZE)
    out_queue = asyncio.Queue()
    dlq = asyncio.Queue()

    consumers = [
        asyncio.ensure_future(
            consumer(
                main_queue=main_queue,
                retry_queue=retry_queue,
                dlq=dlq,
                session=session,
                out_queue=out_queue,
            )
        )
        for _ in range(settings.PROBE.MAX_CONNECTIONS)
    ]

    retry_consumers = [
        asyncio.ensure_future(
            retry_consumer(
                main_queue=retry_queue, dlq=dlq, session=session, out_queue=out_queue
            )
        )
        for _ in range(int(settings.PROBE.MAX_CONNECTIONS / 2))
    ]

    dlq_consumers = [
        asyncio.ensure_future(
            dlq_consumer(main_queue=dlq, dlq=dlq, session=session, out_queue=out_queue)
        )
        for _ in range(settings.PROBE.MAX_CONNECTIONS)
    ]

    out_consumers = [
        asyncio.ensure_future(output_consumer(main_queue=out_queue, dlq=dlq))
        for _ in range(settings.PROBE.MAX_OUTPUT_CONSUMMERS)
    ]

    producer = await produce(queue=main_queue)

    queues_to_wait = (main_queue, retry_queue, dlq, out_queue)
    for q in queues_to_wait:
        await q.join()

    futures = consumers + dlq_consumers + out_consumers + retry_consumers
    for consumer_future in futures:
        consumer_future.cancel()


async def run(loop):
    conn = aiohttp.TCPConnector(limit=settings.PROBE.MAX_CONNECTIONS)

    async with aiohttp.ClientSession(
        loop=loop, connector=conn, trust_env=True
    ) as session:
        await launch(session)


def start():
    # a = {'http_version': '1.1', 'status': 200, 'url': 'http://5plvrsgydwy2sgce.onion', 'real_url': 'http://5plvrsgydwy2sgce.onion', 'content_type': 'text/html', 'charset': 'utf-8'}
    # onion = OnionWebsite(**a, content="lol")
    # return
    os.environ["HTTP_PROXY"] = settings.TOR.HTTP_PROXY
    os.environ["HTTPS_PROXY"] = settings.TOR.HTTPS_PROXY

    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))


if __name__ == "__main__":
    start()
