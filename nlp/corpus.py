import joblib

STOPWORDS_DIR = "./datasets/stopwords"

STOPWORDS = {}


def stopwords(lang):
    if lang in STOPWORDS:
        return STOPWORDS[lang]
    with open(f"{STOPWORDS_DIR}/{lang}", "r") as f:
        data = f.read()
    STOPWORDS[lang] = data.split("\n")
    return STOPWORDS[lang]
    


def _dump_obj(obj, filepath):
    joblib.dump(obj, filepath)


def _load_obj(filepath):
    joblib.load(filepath)
