from bs4 import BeautifulSoup
import polyglot
from polyglot.text import Text, Word
import nltk
import re
from core.helpers.regex import email_regex
from os import walk
from nlp.corpus import stopwords
import string
# pip install pyicu
# pip install pycld2
# pip install morfessor

def tokenize_ntlk(text):
    return nltk.word_tokenize(text)

def tokenize_polyglot(text, lang=None):
    if not text:
        return []
    if lang:
        t = Text(text, hint_language_code=lang)
    else:
        t = Text(text)
    return t.words

def clean_html(raw_html):
    return BeautifulSoup(raw_html).get_text()

def detect_language(text):
    t = Text(text)
    return t.language

def clean_tokens(tokens, lang):
    _stopwords = stopwords(lang)
    def keep(word):
        # Remove integers, emails, phone number, urls ...
        # Remove punctuation
        if word in string.punctuation:
            return False
        # Remove stopwords
        if word in _stopwords:
            return False
        # Remove email addresses
        if email_regex.match(word):
            return False
        # Remove numbers
        if word.isnumeric():
            return False
        return True
    return [ token for token in tokens if keep(token.lower()) ]


def pipeline(raw_html):
    html = clean_html(raw_html)
    print(raw_html)
    print("======================================")
    print(html)
    print("@@@@@@@@@@@@@@@@@@@")
    tokenize_poly = tokenize_polyglot(html)

    tokenized_ntl = tokenize_ntlk(html)
    print(tokenized_ntl)


    print("@@@@@@@@@@@@@@@@@@@")
    print(tokenize_poly)


    print("language = ", detect_language(html))


   

# line = data.split("\n")[0]
# tokens = tokenize_polyglot(line)





# a = clean_tokens(tokens, "fr")
# print(a)


from collections import defaultdict
from gensim import corpora
import operator
from pprint import pprint
frequency = defaultdict(int)



def train_website_classifier():
    corpus_files = []
    corpus_dir = "./datasets/classification/hacking"
    for (dirpath, dirnames, filenames) in walk(corpus_dir):
        corpus_files.extend(filenames)
        break

    
    for corpus_file in corpus_files:

        with open(f"{corpus_dir}/{corpus_file}", "r") as f:
                data = f.read()

        texts = data.split("\n")

        texts_tokens = [ tokenize_polyglot(text, "fr") for text in texts ]
        texts_tokens = [ clean_tokens(tokens, "fr") for tokens in texts_tokens]

        print(len(texts_tokens))


        # Remove words apearing once
        frequency = defaultdict(int)
        for text in texts_tokens:
            for token in text:
                frequency[token] += 1
        texts_tokens = [
            [token for token in text if frequency[token] > 1]
            for text in texts_tokens
        ]
        
        from nlp.classifier import naive_bayes_multinomial
        targets = [0] * len(texts_tokens)

        texts = [" ".join(text) for text in texts_tokens]


        nb = naive_bayes_multinomial(texts, targets)
        print(nb)
        # sorted_d = sorted(frequency.items(), key=operator.itemgetter(1))


        predicted = nb.predict_proba(["je hack des xss de security de ouf hacking"])
        print(predicted)



        break

train_website_classifier()
# dictionary = corpora.Dictionary(final_texts)
# corpus = [dictionary.doc2bow(text) for text in final_texts]

# print("aaaaaaaaaaaaaaadict")
# print(dictionary[0])
# print("aaaaaaaaaaaaaaaaaaacorpus")
# from gensim import models
# tfidf = models.TfidfModel(corpus)  # step 1 -- initialize a model
# print("aaaaaaaaaaaaaaatfidf")
# print(tfidf)

# corpus_tfidf = tfidf[corpus]
# print("aaaaaaaaaaaaaaacorpus_tfidf")
# print(corpus_tfidf)

# for doc in corpus_tfidf:
#     print(doc)

# lsi_model = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=2) 
# corpus_lsi = lsi_model[corpus_tfidf]
# print("===========")
# print(lsi_model.print_topics(2))
# for doc, as_text in zip(corpus_lsi, documents):
#     print(doc, as_text)