from joblib import dump, load


def save_model(model, filepath):
    dump(model, filepath)

def load_model(filepath):
    load(filepath)