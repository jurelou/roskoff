from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB

target_names = ["amossys", "christ"]
target = [0, 0, 1, 1, 1, 1, 0, 0]

text_arr = [
    """
Les CESTI agréés pour les évaluations CC et ITSEC dans le domaine technique logiciels et équipements réseaux sont les suivants1 :

    Amossys [archive] (agréé CESTI depuis novembre 20112)
    Oppida
    """,
    """
Créée en 2007 par des consultants experts de l’industrie de la défense et du monde du service, AMOSSYS est une société de conseil et d’expertise en Sécurité des Technologies de l’information. 
Intrusion Detection Systems (IDS) have made a great progressfor decades but even with the increasing power of AI, we struggle to de-sign a general machine able to detect all kinds of cyber-attacks, especiallythose still unknown a.k.a. zero-day. Indeed, one reason why previous ap-proaches failed can be the complexity of the cyber-security field. How-ever, some research works on anomaly detection have made significativeprogress on aspects related to real-world issues. In particular, the cali-bration of the algorithms does not draw as much attention as their per-formance while all the intelligence can vanish through fine-tuning steps.Here, we tackle issues around the final decision threshold and show howit can be cleverly set.
    """
]

###############################
## NAIVE BAYES multinomial
###############################


def naive_bayes_multinomial(corpus, target):
    text_clf = Pipeline([('vect', CountVectorizer()),
                        ('tfidf', TfidfTransformer()),
                        ('clf', MultinomialNB()),
    ])
    return text_clf.fit(corpus, target)

###############################
## NAIVE BAYES complement
###############################

#TODO: https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.ComplementNB.html#sklearn.naive_bayes.ComplementNB

###############################
## SVM
###############################


# from sklearn.linear_model import SGDClassifier

# text_clf_svm = Pipeline([('vect', CountVectorizer()),
#                         ('tfidf', TfidfTransformer()),
#                         ('clf-svm', SGDClassifier(
#                             loss='hinge',
#                             penalty='l2',
#                             alpha=1e-3,
#                             random_state=42)),
# ])

# text_clf_svm.fit(text_arr, target)




##########################
# test
#########################

# predicted = text_clf.predict_proba(test_data)
# print(predicted[0])
# print("Prediction using naive bayes", predicted)

# predicted_svm = text_clf_svm.predict(test_data)
# print("Prediction using svm", predicted_svm)
