##########################
# language detect using stopwords
#########################
import nltk
from nltk import wordpunct_tokenize
from nltk.corpus import stopwords

def detect_language_sw(text):
    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]
    ratios = {}
    for lang in stopwords.fileids():
        stopwords_set = set(stopwords.words(lang))
        words_set = set(words)
        common_words = words_set.intersection(stopwords_set)

        ratios[lang] = len(common_words)

    def _calc_probability(most, secode_most) :
        if most > 0 and secode_most > 0:
            proba = (float(most) /(most + secode_most) * 100)
            return round(proba)
        return 0

    most_rated_language = max(ratios, key=ratios.get)
    most_common_words = ratios[most_rated_language]
    del ratios[most_rated_language]
    second_most_rated_language = max(ratios, key=ratios.get)
    second_most_common_words = ratios[second_most_rated_language]
    print("STOPWORDS: %s%% chances %s" %(_calc_probability(most_common_words, second_most_common_words), most_rated_language))

##########################
# fasttext
#########################

import fasttext
model = fasttext.load_model('lid.176.bin')

def detect_fasttext(text):
    print("FASTTEXT: ", model.predict(text.split("\n"), k=1))

##########################
# textcat
##########################

import nltk
import pycountry
from nltk.stem import SnowballStemmer

nltk.download('crubadan')
def detect_textcat(text):

    tc = nltk.classify.textcat.TextCat() 
    guess = tc.guess_language(text)
    dist = tc.lang_dists(text)

    guess_name = pycountry.languages.get(alpha_3=guess).name
    print("TEXTCAT:", guess_name)

##########################
# chardet
#########################

import chardet
def detect_chardet(text):
    print("CHARDET:", chardet.detect(text.encode()))

##########################
# test
#########################

test_text = """
entièrement entré dans la cage bordelaise au moment où le gardien Cédric Carrasso se saisissait de celui-ci,
hello my name is something i cant pronounce
"""

test_cn = """
天津津伯新技术有限公司位于工业基地的天津市，是集电动执行机构的研究开发、生产制造、销售、安装调试于一体的专业单位。本公司吸收国内外同行业的先进技术，结合当下市场需求，研制开发出更适合市场需求的高品质产品。先进的超大规模的数字集成芯片，专业的数字力矩传感器和数字位移传感器，中英文可切换菜单，满足国内外客户的需求。天津津伯生产的多回转、角行程，直行程智能型电动执行机构与各种阀门配套，组成执行单元。广泛用于电力、石油、煤化工、天然气、长输仓储、冶金冶炼、造纸、建材、市政环保等汽液输送管道的自动控制系统中，既能满足频繁调节控制，又能满足断续控制的要求。可以更广泛的为不同领域的使用需求提供解决方案。天津津伯将不断的完善自己，不忘初心，励志前行。我们愿与各界朋友共同努力，为中国自动化控制领域做出我们的贡献，并期盼与您合作共谋发展！ 
"""

test_ar = """
كبر، ويسمى بعلم النظر والاستدلال، ويسمى أيضًا بعلم التوحيد والصفات، وفي شرح العقائد النسفية لسعد الدين التفتازاني: العلم المتعلق بالأحكام الفرعية أي العلمية يسمى علم الشرائع والأحكام، وبالأحكام الأصلية أي الاعتقادية يسمى علم التوحيد والصفات. وسُمِّي بعلم التوحيد؛ لأن مبحث الوحدانية أشهر مباحثه، وسمي بعلم أصول الدين؛ لابتناء الدين عليه. فهو يبحث في أصول الدين العلمية، بخلاف علم الفقه فإنه يبحث في الأحكام الفرعية العملية (المتعلقة بفعل المكلف). ويسمى بعلم الذات والصفات؛ لأنه يهتم بموضوع معرفة صفات الله تعالى، والتي تعد من أهم مباحثه. ويقوم علم الكلام على بحث ودراسة مسائل العقيدة الإسلامية بإيراد الأدلة وعرض الحجج على إثباتها، ومناقشة الأقوال والآراء المخالفة لها، وإثبات بطلانها، ودحض ونقد الشبهات التي تثار حولها، ودفعها بالحجة والبرهان. كالاستدلال على ثبوت وجود خالق الكون، وثبوت أنه واحد لا شريك له، يُرجع إلى هذا العلم، وعن طريقه يتم التعرف على الأدلة التي يوردها العلماء في هذا المجال. وذلك أن هذا العلم هو ال
"""

test_ru = """
Я люблю вкусные пампушки
"""
def _test(text, lang):
    print("==================\nTESTING ", lang)
    detect_language_sw(text)
    detect_textcat(text)
    detect_chardet(text)
    detect_fasttext(text)
_test(test_text, "french-english")
_test(test_cn, "chinese")
_test(test_ar, "arabic")
_test(test_ru, "russian")
