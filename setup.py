from setuptools import find_packages, setup

setup(
    name="roskoff",
    version="0.0.1",
    description="",
    long_description="",
    author="",
    author_email="",
    url="",
    scripts=["bin/build_infra"],
    entry_points={"console_scripts": ["roskoff=bin.roskoff:main"],},
    packages=find_packages(),
    install_requires=[
        "elasticsearch==7.8.0",
        "scrapy==2.2.1",
        "beautifulsoup4==4.9.1",
        "neo4j==4.0.2",
        "dynaconf[yaml]==3.0.0",
        "fire==0.3.1",
        "asyncio==3.4.3",
        "async-timeout==3.0.1",
        "aiohttp==3.6.2",
        "aiodns==2.0.0",
        "cchardet==2.1.6",
        "httpx==0.12.1",
        "nltk>=3.5",
        "scikit-learn>=0.23.2"
    ],
    extras_require={},
    python_requires=">=3.7.*, <4",
)
