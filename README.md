# Requirements

* python3.7
* python3.7-venv (or the virtual environment of your choice)

# Setup

## Base installation

This is the base setup for every python project ..

```sh
python3.7 -m venv env
source env/bin/activate
pip install pip setuptools wheel -U # this may not be needed depending of your distro
pip install .
```
## Simple setup


```sh
build_infra
```


This will build everything for you.

## Manual setup

* Create the backend infrastructure

```
docker-compose -f ./docker/docker-compose-backend.yml up 
```


* Create a cluster of tor clients

```
docker-compose -f ./docker/docker-compose-multitor.yml --compatibility up
```

test the tor clients using:
```
curl --proxy localhost:8118 http://httpbin.org/ip
curl --socks5-hostname localhost:5566 http://httpbin.org/ip
```
Services available:


* http://127.0.0.1:8118 : tor clients http proxy
* http://127.0.0.1:5566 : tor clients socks proxy
* http://127.0.0.1:5500 : haproxy stats interface
* http://127.0.0.1:5601 : kibana interface
* http://127.0.0.1:7474 : neo4j interface



# Usage

Start by initializing the database

```
roskoff core init
```

then a wide range of choices is available to you .....

```
roskoff crawl hn
...
```


```
roskoff -h 
```

# How to

Gather a few data:

```
roskoff core reset && roskoff crawl test "https://darknetlive.com/markets" && roskoff probe
```

(Run `roskoff core clean` to clean the database.)



# Notes

https://onlinelibrary.wiley.com/doi/full/10.1002/1097-4571(2000)9999:9999%3C::AID-ASI1591%3E3.0.CO;2-R

https://www.elastic.co/fr/blog/significant-terms-aggregation#classifier

https://stackoverflow.com/questions/49660989/searching-english-text-in-html-using-elasticsearch

https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/OMV93V

https://github.com/ContentHolmes/Website-Classifier-Native/blob/master/FeatureExtraction/urlWordsExtractor.py

https://github.com/domantasm96/URL-categorization-using-machine-learning

language analyzers
https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html

https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html

https://www.nltk.org/_modules/nltk/stem/snowball.html

https://www.nltk.org/book/ch03.html


https://towardsdatascience.com/machine-learning-nlp-text-classification-using-scikit-learn-python-and-nltk-c52b92a7c73a

# DATASETS:

* hacking / security / it 

https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/1TCFII

http://kdd.ics.uci.edu/databases/kddcup99/training_attack_types

https://attack.mitre.org/

* markets

https://www.kaggle.com/philipjames11/dark-net-marketplace-drug-data-agora-20142015

* drugs

https://www.kaggle.com/everling/cocaine-listings


# TODO:

* In website model split sheme and netloc  ??

* dorks:

"http?://*.onion" OR "http?://*.onion.tor2web" OR "http?://*.tor2web.org" OR "http?://*.onion.cab/"
site:websitedown.info/*.onion OR site:websitenotworking.com/*.onion


* Reuse the tor circuits. Maybe using random and seed(url)

* broad crawler: allow to define max depth for each link

* scrapy manage non 200 return codes

* dorks using https://github.com/MarioVilas/googlesearch
http://web.archive.org/web/20140625145138/http://www.rankpanel.com/blog/google-search-parameters/
https://dev.to/muhajirdev/scraping-from-google-visualizing-most-common-word-with-wordcloud-4mko


* elasticsearch bulk index

https://www.elastic.co/guide/en/elasticsearch/reference/current/tune-for-indexing-speed.html


* Kibana refresh index pattern (remove + create if not possible)

https://github.com/elastic/kibana/issues/6498



parse wikipedia

extraire les phrases


sentences = nltk.sent_tokenize(textsample)  
