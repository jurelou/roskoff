import re

onion_regex = re.compile(
    r"((?:https?\:\/\/)?(?:[a-z2-7]{16}|[a-z2-7]{56})\.onion)", re.IGNORECASE
)

email_regex = re.compile(r"[^@\s]+@[^@\s]+\.[a-zA-Z0-9]", re.IGNORECASE)