from hashlib import blake2b


def blake2(data: str, digest_size=10):
    h = blake2b(digest_size=digest_size)
    h.update(data.encode())
    return h.hexdigest()
