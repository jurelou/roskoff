import re
from core.helpers.regex import onion_regex


def find_onions(text):
    onions = onion_regex.findall(text)
    return set(onions)


def is_onion(text):
    return onion_regex.match(text)
