from dataclasses import dataclass


@dataclass
class BaseElasticModel:
    @classmethod
    def index(cls):
        index = cls.__name__.lower()
        if index.endswith("model"):
            return index[:-5]
        return index
