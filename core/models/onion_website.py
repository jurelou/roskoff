import dataclasses

from core.models import BaseElasticModel
from core.models.feed import Feed
from core.helpers.tor import onion_regex
from datetime import datetime

from nlp.pipeline import pipeline as nlp_pipeline

@dataclasses.dataclass
class OnionWebsite(BaseElasticModel):
    """Describes a Website.
    url: Address of the website (eg. 'https://example.com')
    feed: Origin of the discovery of the URL. Should be one of core.objects.feed.Feed
    """

    http_version: str
    status: int
    html: str
    url: str
    real_url: str
    content_type: str
    charset: str

    bite: str = ""

    last_update: datetime = datetime.now()
    # headers : str
    # source: Feed = Feed.UNDEFINED


    def __post_init__(self):
        self.bite = "lol"
        # self.last_update = datetime.now()
        nlp_pipeline(self.html)

    def to_dict(self):
        d = dataclasses.asdict(self)
        d["source"] = d["source"].name
        return d
