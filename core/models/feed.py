from enum import Enum


class Feed(Enum):
    UNDEFINED = 0
    BROAD_CRAWLING = 1
    HACKER_NEWS = 2
    SURFACE = 3
    THE_HIDDEN_WIKI = 4
