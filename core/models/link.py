import dataclasses
from enum import Enum
import urllib.parse

from core.models import BaseElasticModel


class Relationship(Enum):
    UNDEFINED = 0
    STRAIGHT = 1  # The `source` website directly points to the `target` (eg. via href).
    DERIVED = (
        2  # The `source` website contains an indirect url to `target` (eg. via <h1> ).
    )
    SAME_SITE = 3  # Both `source` and `target` are hosted on the same domain name.


@dataclasses.dataclass
class Link(BaseElasticModel):
    """Describes a link between two urls."""

    source: str
    target: str
    relationship: Relationship = Relationship.UNDEFINED

    def __post_init__(self):
        """Checks if relationship is of type `SAME_SITE`."""
        source_url = urllib.parse.urlsplit(self.source).hostname
        if source_url and source_url == urllib.parse.urlsplit(self.target).hostname:
            self.relationship = Relationship.SAME_SITE

    def to_dict(self):
        d = dataclasses.asdict(self)
        d["relationship"] = d["relationship"].name
        return d
