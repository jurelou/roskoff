import dataclasses

from core.models import BaseElasticModel
from core.models.feed import Feed
from core.helpers.tor import is_onion


@dataclasses.dataclass
class Website(BaseElasticModel):
    """Describes a Website.
    url: Address of the website (eg. 'https://example.com')
    feed: Origin of the discovery of the URL. Should be one of core.objects.feed.Feed
    """

    url: str
    source: Feed = Feed.UNDEFINED

    def __post_init__(self):
        if self.url.endswith("/"):
            self.url = self.url[:-1]

    def to_dict(self):
        d = dataclasses.asdict(self)
        d["source"] = d["source"].name
        return d

    def is_onion(self):
        return True if is_onion(self.url) else False
