from core.database import es
import dataclasses
from .base import BaseService
from core.models import Website
from elasticsearch import ConflictError

from core.helpers.crypto import blake2


class WebsiteService(BaseService):
    model = Website

    # @classmethod
    # def get(cls, url):
    #     pass

    def find_by_url(self, url):
        return es.get(index=self.index_name, id=blake2(url))

    def index(self, body: Website, **kwargs):
        _website = dataclasses.asdict(body)
        _website["source"] = body.source.name
        _website["is_tor"] = body.is_onion()
        try:
            es.index(
                index=self.index_name,
                body=_website,
                id=blake2(_website["url"]),
                op_type="create",
            )
        except ConflictError:
            pass

    def get_elastic_mapping(self):
        mapping = super(WebsiteService, self).get_elastic_mapping()
        mapping["mappings"]["properties"]["is_tor"] = {"type": "boolean"}
        return mapping

    def scroll_onions(self, **kwargs):
        return self.scroll(q={"query": {"match": {"is_tor": True}}}, **kwargs)
