from core.database import es
from elasticsearch.helpers import scan
from dynaconf import settings
import dataclasses
import httpx

class BaseService:

    _index_name = None

    def __init__(self, **kwargs):
        if not hasattr(self, "model"):
            raise AttributeError(
                f"Service {self.__class__.__name__} should have a `model` attribute."
            )

    def create_index(self):
        es.indices.create(index=self.index_name, body=self.get_elastic_mapping())
        es.indices.put_settings(
            index=self.index_name,
            body={"refresh_interval": "30s", "number_of_replicas": 0},
        )
        # Create kibana pattern
        kibana_endpoint = f"{settings.KIBANA.URL}/api/saved_objects/index-pattern/{self.index_name}"
        headers = {"kbn-xsrf": "yes", "Content-Type": "application/json"}
        data = {
            "attributes": {
                "title": f"{self.index_name}*"
            }
        }
        r = httpx.post(kibana_endpoint, json=data, headers=headers)
        print(f"Kibana create index pattern: {r.status_code}")

    def remove_index(self):
        es.indices.delete(index=self.index_name)

    @property
    def index_name(self):
        return self._index_name if self._index_name else self.model.index()

    def get(self, **kwargs):
        pass

    def index(self, body, **kwargs):
        es.index(index=self.index_name, body=dataclasses.asdict(body), **kwargs)

    def refresh(self, **kwargs):
        es.indices.refresh(index=self.index_name, **kwargs)

    def scroll(self, size=100, q=None, **kwargs):
        if not q:
            q = {"query": {"match_all": {}}}
        return scan(es, size=size, query=q, index=self.index_name, **kwargs)

    def get_elastic_mapping(self):
        print(f"get mapping for {self.index_name}")
        mapping = {"properties": {}}
        mapping_types = {str: "text", int: "integer"}
        for field in self.model.__dataclass_fields__.values():
            if field.type in mapping_types:
                print(
                    f"  field {field.name} type {field.type} maps to {mapping_types[field.type]}"
                )
                _type = mapping_types[field.type]
            else:
                print(f"  field {field.name} type {field.type} maps to default (text)")
                _type = "text"
            mapping["properties"][field.name] = {"type": _type}
        return {"mappings": mapping}
