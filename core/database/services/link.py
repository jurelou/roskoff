from core.database import es
import dataclasses
from .base import BaseService
from core.models import Link


class LinkService(BaseService):
    model = Link

    def find_link(self, source, target):
        q = {
            "query": {
                "match": {"source": {"query": source}},
                "match": {"target": {"query": target}},
            }
        }
        return es.search(index=self.index_name, body=q)["hits"]["hits"]

    def index(self, body: Link, **kwargs):
        # if self.find_link(source=body.source, target=body.target):
        #     return
        body = dataclasses.asdict(body)
        body["relationship"] = body["relationship"].name
        es.index(index=self.index_name, body=body)
