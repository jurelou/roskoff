from core.database import es
import dataclasses
from .base import BaseService
from core.models import OnionWebsite
from elasticsearch import ConflictError

from core.helpers.crypto import blake2
from core.database.pipelines import HtmlStripPipeline

class OnionWebsiteService(BaseService):
    model = OnionWebsite

    # @classmethod
    # def get(cls, url):
    #     pass
    _index_name = "onion-ws-tmp"

    def find_by_url(self, url):
        return es.get(index=self.index_name, id=blake2(url))

    def index(self, body: OnionWebsite, **kwargs):
        _body = dataclasses.asdict(body)

        # _website["url"] = body.source.name
        try:
            es.index(
                index=self.index_name,
                body=_body,
                id=blake2(_body["url"]),
                op_type="create",
            )
        except ConflictError:
            pass

    def get_elastic_mapping(self):
        mapping = super().get_elastic_mapping()

        url_mapping = {
            "type": "keyword",
        }

        mapping["mappings"]["properties"]["url"] = {
            "type": "keyword",
        }

        print("FINAL MAPPING", mapping)        
        return mapping

    # def get_elastic_mapping(self):
    #     mapping = super().get_elastic_mapping()
    #     mapping["settings"] = {
    #         "analysis": {s
    #             ##############
    #             # Filters
    #             #############
    #             "filter": {

    #                 # filter_shingle
    #                 "filter_shingle":{
    #                     "type":"shingle",
    #                     "max_shingle_size":5,
    #                     "min_shingle_size":2,
    #                     "output_unigrams":False
    #                 }

    #             },
    #             ##############
    #             # Analysers
    #             #############
    #             "analyzer": {


    #                 "toto": {
    #                     "type":"custom",
    #                     "tokenizer":"standard",
    #                     "filter": [
    #                         "lowercase"
    #                     ]
    #                 },

    #                 # raw_html
    #                 "raw_html": {
    #                     "tokenizer": "standard",
    #                     "type": "custom",
    #                     "char_filter": [
    #                         "html_strip",
    #                         # "quotes"
    #                     ],
    #                     "filter": [
    #                         "lowercase",
    #                         # "stop"
    #                     ]
    #                 },
    #                 # shingle_html
    #                 "shingle_html": {
    #                     "tokenizer": "standard",
    #                     "filter": [
    #                         "lowercase",
    #                         "filter_shingle",

    #                     ],

    #                     # "type": "custom",
    #                     # "char_filter": [
    #                     #     "html_strip",
    #                     #     "quotes"
    #                     # ],

    #                 },
    #                 # "toto": {
    #                 #     "type":"custom",
    #                 #     "tokenizer":"standard",
    #                 #     "filter":[
    #                 #         "lowercase",
    #                 #         "stop"
    #                 #     ],
    #                 #     "char_filter":[
    #                 #         "html_strip",
    #                 #         "quotes"
    #                 #     ],
    #                 # }
    #             }
    #         }
    #     }
    #     html_mapping = {
    #         "type": "text",
    #         "analyzer": "raw_html",
    #         "fields": {
    #             "shingles": {
    #                 "type": "text",
    #                 "analyzer": "shingle_html"
    #             },
    #             "kwhtml": {
    #                 "type": "keyword"
    #             }

    #             # "english": {
    #             # "type": "text",
    #             # "analyzer": "english_html"
    #             # }
    #         }

    #     }
    #     url_mapping = {
    #         "type": "text",
    #         "fields": {
    #             "url_bite": {
    #                 "type": "keyword"
    #             }
    #         }
    #     }
    #     mapping["mappings"]["properties"]["html"] = html_mapping
    #     mapping["mappings"]["properties"]["url"] = url_mapping

    #     print("FINAL MAPPING", mapping)

    #     return mapping
