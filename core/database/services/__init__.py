from .website import WebsiteService
from .link import LinkService
from .onion_website import OnionWebsiteService

WebsiteService = WebsiteService()
LinkService = LinkService()
OnionWebsiteService = OnionWebsiteService()

__all__ = [LinkService, WebsiteService, OnionWebsiteService]
