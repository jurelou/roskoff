from core.helpers.singleton import Singleton
from neo4j import GraphDatabase
import neo4j.exceptions
from dynaconf import settings


NEO_SETTINGS = settings.NEO4J


class Neo4jDriver(metaclass=Singleton):
    def __init__(self):
        self._driver = GraphDatabase.driver(
            NEO_SETTINGS.uri, auth=(NEO_SETTINGS.user, NEO_SETTINGS.password)
        )
        with self._driver.session() as session:
            # Drop all constraints
            for constraint in session.run("CALL db.constraints"):
                print(constraint)
                session.run("DROP CONSTRAINT " + constraint[0])
            # Create constraint: unique website url
            session.run("CREATE CONSTRAINT ON (n:Website) ASSERT n.url IS UNIQUE;")

    @staticmethod
    def _run_tx(tx, toto):
        pass

    def __del__(self):
        try:
            self._driver.close()
        except AttributeError:  # Could not initiate a connection to neo
            pass
