from .html_strip import HtmlStripPipeline

HtmlStripPipeline = HtmlStripPipeline()

__all__ = [
    HtmlStripPipeline,
]
