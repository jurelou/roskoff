from core.database import es
from .base import BasePipeline


class HtmlStripPipeline(BasePipeline):
    _pipeline_id = "html_strip"

    def create(self):
        p = {
            "description": "Removes HTML tags from the `html` field.",
            "processors": [
                # {
                #     "date" : {
                #         "field" : "initial_date",
                #         "target_field" : "timestamp",
                #         "formats" : ["dd/MM/yyyy HH:mm:ss"],
                #         "timezone" : "Europe/Amsterdam"
                #     }
                # }

                {
                    "html_strip": {
                        "field": "html",
                        "target_field": "striped_html",
                        "tag": "html_strip",
                    }
                },
                # {
                #     "trim": {
                #         "field": "striped_html"
                #     }
                # },
                {
                    "urldecode": {
                        "field": "url"
                    }
                },
                # {
                #     "uppercase": {
                #         "field": "real_url"
                #     }
                # },
        {
            "inference": {
            "model_id": "lang_ident_model_1",
            "inference_config": {
                "classification": {
                "num_top_classes": 3
                }
            },
            "field_map": {
                "html": "text"
            },
            "target_field": "_ml.lang_ident"
            }
        },



            ],
        }
        if self.exists():
            print(f"Pipeline {self.identifier} already exists")
        else:
            es.ingest.put_pipeline(id=self.identifier, body=p)
            print(f"Create {self.identifier} pipeline")
