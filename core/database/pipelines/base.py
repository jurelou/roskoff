from core.database import es
from elasticsearch import NotFoundError


class BasePipeline:

    _pipeline_id = None

    @property
    def identifier(self):
        return self._pipeline_id

    @identifier.setter
    def identifier(self, identifier):
        self._pipeline_id = identifier

    def exists(self):
        try:
            es.ingest.get_pipeline(id=self.identifier)
            return True
        except NotFoundError:
            return False

    def get(self):
        return es.ingest.get_pipeline(id=self.identifier)

    def create(self):
        raise NotImplementedError(
            f"Class {type(self).__name__} should have a `create` method"
        )

    def delete(self):
        if self.exists():
            es.ingest.delete_pipeline(id=self.identifier)
            print(f"Delete pipeline {self.identifier}")
