from elasticsearch import Elasticsearch
import logging
from dynaconf import settings

es = Elasticsearch(maxsize=25)
es_logger = logging.getLogger("elasticsearch")
es_logger.setLevel(settings.ELASTICSEARCH.log_level)

from . import services  # noqa
from . import pipelines  # noqa


def bootstrap():
    print("Bootstraping")
    for service in services.__all__:
        if not es.indices.exists(index=service.index_name):
            print("CREATE")
            service.create_index()
        else:
            print(f"Index {service.index_name} already exists")

    # for pipeline in pipelines.__all__:
    #     pipeline.create()


def clean():
    for service in services.__all__:
        if es.indices.exists(index=service.index_name):
            print(f"Remove index {service.index_name}")
            service.remove_index()

    # for pipeline in pipelines.__all__:
    #     pipeline.delete()
