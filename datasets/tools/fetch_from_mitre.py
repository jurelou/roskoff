# extract texts from https://github.com/mitre/cti
# Requires https://github.com/mitre/cti to be installed in the root directory
# Otherwise u can modify the path for `files`
from os import walk
import json


files = [
    "cti/pre-attack/pre-attack.json",
    "cti/mobile-attack/mobile-attack.json",
    "cti/enterprise-attack/enterprise-attack.json",
]

output_file = "./datasets/classification/hacking/mitre.txt"
with open(output_file, "w") as out_file:
    for f in files:
        with open(f, "r") as stix_file:
            stix_data = json.load(stix_file)
            for obj in stix_data["objects"]:
                if "description" in obj:
                    out_file.write(obj["description"])
