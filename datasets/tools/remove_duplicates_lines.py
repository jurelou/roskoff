import os

files = []

STOPWORDS_DIR = "./datasets/stopwords"

for (_, _, filenames) in os.walk(STOPWORDS_DIR):
    files.extend([f"{STOPWORDS_DIR}/{f}" for f in filenames])
    break

print("files:", files)
for f in files:
    lines = []

    with open(f, "r") as data:
        all_lines = data.readlines()
        lines = set(all_lines)
        print(f"File {f}, removed {len(all_lines) - len(lines)} lines")


    with open(f, "w") as f_data:
        for line in lines:
            f_data.write(line)

print("Found files:", files)