import httpx
import json
from bs4 import BeautifulSoup


URLS_FILE = "./datasets/classification/wikipedia_urls.json"
OUTPUT_DIR = "./datasets/classification"

def download_page(url):
    r = httpx.get(url)
    if r.status_code == 200:
        return r.text
    print(f"Could not retrieve {url} error {r.status_code}")
    return None

def extract_wikipedia_content(raw_html):
    soup = BeautifulSoup(raw_html, features="lxml")
    content = soup.find(id="bodyContent")
    return content.get_text()

def extract_lines(text):
    lines = []
    for line in text.split("\n"):
        line = line.strip()
        if line:
            lines.append(line)
    return lines



with open(URLS_FILE) as urls_file:
    data = json.load(urls_file)


hacking_urls = data["hacking"]
for lang in hacking_urls.keys():
    output_file = f"{OUTPUT_DIR}/hacking/{lang}.txt"
    with open(output_file, "w+") as out:
        for hacking_url in hacking_urls[lang]:
            raw_html = download_page(hacking_url)
            content = extract_wikipedia_content(raw_html)
            out.write("\n".join(extract_lines(content)))
