#!/bin/bash

SUBNET_PREFIX=$(hostname -I | cut -f1-3 -d".")

function ping_tor() {
  echo "PING $1"
  netcat -zvw10 $1 8050 &> /dev/null
  if [ $? -eq 0 ]; then
      SERVER_NAME=$(echo $1 | cut -f4 -d".")
      echo "Adding backend (tor_$SERVER_NAME) ip $1 port 9050"
      echo "  server tor_$SERVER_NAME $1:9050" >> /usr/local/etc/haproxy/haproxy.cfg
  fi
}

sleep 4
export -f ping_tor

cat /usr/local/etc/haproxy/haproxy.cfg.tmpl > /usr/local/etc/haproxy/haproxy.cfg
echo "Reset haproxy conf"
cat /usr/local/etc/haproxy/haproxy.cfg
fping -a -g $SUBNET_PREFIX.0/24 2>/dev/null | xargs -n 1 -P2 -I {} bash -c 'ping_tor "$@"' _ {}

cat /usr/local/etc/haproxy/haproxy.cfg

haproxy  -db -V -f /usr/local/etc/haproxy/haproxy.cfg
echo "done bootstraping"

